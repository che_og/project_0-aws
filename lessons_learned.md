# Environment Variables
TPZ_USERNAME and TPZ_PASSWORD need to be specified in the runcmd
The following don't work
- Putting `export TPZ_USERNAME/PASSWORD` in /etc/environment
- Putting `export TPZ_USERNAME/PASSWORD` in /home/che/.bashrc
- Putting `export TPZ_USERNAME/PASSWORD` in /home/che/.profile
- Putting `export TPZ_USERNAME/PASSWORD` in /home/root/.profile

## Note about .bashrc/.profile
.profile is sourced and .bashrc isn't


# Cloud-init debugging
## Logs:

The logs are accessible a few different ways

- /var/log/cloud-init.log
- /var/log/cloud-init-output.log
- journalctl --unit cloud-init
- cloud-init collect-logs
- cloud-init analyze

Of these the most helpful tend to be `/var/log/cloud-init-output.log`. It
tends to have the most clear output. It has the stdout and maybe stderr from
commands.

## Status and Waiting:
`cloud-init status --wait`  
Will print dots about twice a second until cloud-init is done running. Extremely
helpful when I need to call `remote-exec`.

`cloud-init status --long`  
Prints out any cloud-init error messages if they happen. So, for example, when
you try to set owner as che:che on a write_files the error from that will show
up in this message.

## Validation:
`cloud-init devel schema --annotate --config me_user-data`  
That bad boy above will do a best effort at making sure your user-data is
valid.

## The `write_files`  Module:
The write_files module runs before the users module. That means that I can't
put che:che as owner if che:che isn't a user in the image. You'll get an error
but the file will still be written just under root:root.

Making a User The Azure/Terraform Way:
When you specify a user via `admin_username` the users home directory is owned
by root. This is honestly prolly cause of the whole write_files thing.

# Lessons Learned

## Using `cloud-init`:

### Cloud-init is best used on things that are not dependent on each other.

You can't effectively express order or dependency or carry even set environment
variables easily. So when I need a user so that I can write a file, for example,
that just simply will not work. Best I can do is write the file then in the
runcmd do a chown.

### When using cloud-init it's best to use the builtin modules.

The runcmd stuff is hacky. You already know that.

### Dump state when using runcmd.

The logs show what was output from a script or command. Therefore you should
dump state early so it shows up in the logs and you have context for debugging
the rest.

### Add some bash error handling and shit.

Don't be afraid to do a little `command || investigation && false` in the
command so that if it does fail you get a little more useful error message.

### Avoid pipes in runcmd.

In this same vein as the rest AVOID PIPES. Pipes hide what's going on and fuck
my shit right up. I woulda saved myself like 4 to 5 hours if I didn't have
pipes.

### Don't Use cloud-init or remote-exec:

I would highly recommend that for any setup that has any kind of complexity,
use packer with an ansible provisioner.
NOT remote-exec
NOT cloud-init

cloud-init cannot handle complexity. It has bad debugging, very few builtin
modules, and no ordering. I very quickly found myself needing runcmd which
gets messy.

remote-exec fails to provision which stops the whole terraform process every
time it get's a little baby hickup. Also it is just running bash which is bad.

Ansible is way better because it has order, builtin modules, and it's just
competent. Then putting that shit in packer is prime because now you can just
specify the image from terraform and you don't have to nervously see if the
shits gonna run properly every time. It already did. It's already done.

In the example of this submitter I think I would install the packages, make
the user, put the ssh key in place, (not do ssh shinanigans cause it's not
that deep), then make install systemd that will call a python script on boot.
The python script checks a file for the domain name then calls submit and has
nice verbose logging. Then, since the image has your user, cloud-init can
write_files a file with the domain name to your user and bamo.

"Oh but I need sensative variables" well it's time to look at a key value
store that your services can query on boot.

I will need to learn how to use ansible better however.
