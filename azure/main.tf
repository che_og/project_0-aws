variable "pub_key" {
  type        = string
  description = "SSH public key filename"
  default     = "/home/che/.ssh/id_rsa.pub"
}

variable "priv_key" {
  type        = string
  description = "SSH private key filename"
  default     = "/home/che/.ssh/id_rsa"
}

variable "username" {
  type        = string
  description = "Username for ssh and such"
  default     = "che"
}

variable "tpz_username" {
  type        = string
  description = "Username for The Project Zone (to turn in assignment)"
  default     = "colavarr@andrew.cmu.edu"
}

variable "tpz_password" {
  type        = string
  description = "Password for The Project Zone (to turn in assignment)"
  default     = "2gtozqZwQPuwvOo3Gmdonl"
}


terraform {
  backend "remote" {
    organization = "cloud-computing"
    workspaces {
      name = "project-0_azure"
    }
  }
}

# ARM_CLIENT_ID and ARM_CLIENT_SECRET
provider "azurerm" {
  features {}
  # subscription_id = "fb34945d-b50f-41c5-a46d-f7e059b603b7"
  # tenant_id = "eff9e863-33ef-479e-99f8-330659dfb09e"
}

data "azurerm_resource_group" "project" {
  name = "Project_0"
}

resource "random_id" "almost_suffix" {
  byte_length = 4
}

locals {
  suffix     = replace(random_id.almost_suffix.b64_url, "_", ".")
  dns_suffix = substr(random_id.almost_suffix.hex, 0, 4)
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                = "vm-${local.suffix}"
  location            = data.azurerm_resource_group.project.location
  resource_group_name = data.azurerm_resource_group.project.name
  size                = "Standard_B1ms"

  network_interface_ids = [azurerm_network_interface.net_interface.id]
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts"
    version   = "latest"
  }

  admin_username = var.username
  admin_ssh_key {
    username   = var.username
    public_key = file(var.pub_key)
  }

  custom_data = base64encode(templatefile(
    "user-data.yaml", {
      username     = var.username
      tpz_username = var.tpz_username
      tpz_password = var.tpz_password
      dns_name     = azurerm_public_ip.ip.fqdn
    }
  ))
}

resource "azurerm_network_interface_security_group_association" "ni-sg-assoc" {
  network_interface_id      = azurerm_network_interface.net_interface.id
  network_security_group_id = azurerm_network_security_group.sg.id
}

resource "azurerm_network_security_group" "sg" {
  name                = "sg-${local.suffix}"
  location            = data.azurerm_resource_group.project.location
  resource_group_name = data.azurerm_resource_group.project.name

  security_rule {
    name                       = "SSH"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "http"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "https"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_public_ip" "ip" {
  name                = "ip-${local.suffix}"
  resource_group_name = data.azurerm_resource_group.project.name
  location            = data.azurerm_resource_group.project.location
  allocation_method   = "Dynamic"
  domain_name_label   = "me-${local.dns_suffix}"
}


resource "azurerm_virtual_network" "network" {
  name                = "network-${local.suffix}"
  location            = data.azurerm_resource_group.project.location
  resource_group_name = data.azurerm_resource_group.project.name
  address_space       = ["10.0.0.0/16"] # what does this mean (CIDR block)
  subnet {
    name           = "subnet-${local.suffix}"
    address_prefix = "10.0.1.0/24"
  }
}

resource "azurerm_network_interface" "net_interface" {
  name                = "network_interface-${local.suffix}"
  location            = data.azurerm_resource_group.project.location
  resource_group_name = data.azurerm_resource_group.project.name

  ip_configuration {
    name                          = "ip_config_name"
    subnet_id                     = tolist(azurerm_virtual_network.network.subnet)[0].id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ip.id
  }
}

output "ip" {
  value = azurerm_linux_virtual_machine.vm.public_ip_address
}

output "dns_name" {
  value = azurerm_public_ip.ip.fqdn
}
