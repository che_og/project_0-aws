variable "pub_key" {
  type = string
  description = "SSH public key filename"
  default = "/home/che/.ssh/id_rsa.pub"
}

variable "priv_key" {
  type = string
  description = "SSH private key filename"
  default = "/home/che/.ssh/id_rsa"
}

variable "project_num" {
  type = string
  description = "Project number to use for tags"
  default = "0"
}

variable "tpz_username" {
  type = string
}

variable "tpz_password" {
  type = string
}

terraform {
  backend "remote" {
    organization = "cloud-computing"
    workspaces {
      name = "project-0_aws"
    }
  }
}

# using environment vars: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
provider "aws" {
  region = "us-east-1"
}

resource "aws_spot_instance_request" "me-instance" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  spot_price             = "0.01"
  wait_for_fulfillment   = true
  vpc_security_group_ids = [aws_security_group.ssh.id]
  key_name               = aws_key_pair.ssh-key.key_name

  root_block_device {
    volume_size = "30"
    volume_type = "gp2"
    tags = {
      Project = var.project_num
    }
  }

  # Tag the request as I'm tagging everything and its dog with project label
  tags = {
    Project = var.project_num
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file(var.priv_key)
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo add-apt-repository \"deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse\"",
      "sudo apt update",
      "sudo apt install apache2 wget -y"
    ]
  }

  provisioner "file" {
    destination = "/tmp/index.html"
    content = "Cloud Computing is awesome!"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/index.html /var/www/html/index.html",
      "sudo chown root:root /var/www/html/index.html",
      "wget clouddeveloper.blob.core.windows.net/s21-15619/aws-playground/submitter -O /home/ubuntu/submitter",
      "chmod 744 /home/ubuntu/submitter",
      "TPZ_USERNAME=${var.tpz_username} TPZ_PASSWORD=${var.tpz_password} bash -c 'echo \"I AGREE\" | /home/ubuntu/submitter ${self.public_dns}'",
    ]
  }
}

resource "aws_ec2_tag" "project-tag" {
  resource_id = aws_spot_instance_request.me-instance.spot_instance_id
  key         = "Project"
  value       = var.project_num
}

resource "aws_key_pair" "ssh-key" {
  public_key = file(var.pub_key)
  tags = {
    Project = var.project_num
  }
}

# The ami changes depending on the region. Even though I have a fixed region
# right now I think it's best to make that more general.
#
# Below will get a single ami name (the search terms might return a list but it
# will pick the first thing off the top).
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_default_vpc" "me-vpc" {}

resource "aws_security_group" "ssh" {
  name        = "allow-ssh"
  description = "Just the ingress blocking everything but ssh"
  vpc_id      = aws_default_vpc.me-vpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Apache Server"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Egress rules are necessary or you cant `apt update` `ping` or anything.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Project = var.project_num
  }
}

output "ami_name" {
  value = data.aws_ami.ubuntu.name
}

output "ip" {
  value = aws_spot_instance_request.me-instance.public_ip
}
