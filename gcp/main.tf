variable "pub_key" {
  type        = string
  description = "SSH public key filename"
  default     = "/home/che/.ssh/id_rsa.pub"
}

variable "priv_key" {
  type = string
  description = "SSH private key filename"
  default = "/home/che/.ssh/id_rsa"
}

variable "project_num" {
  type        = string
  description = "Project number to use for tags"
  default     = "0"
}

variable "username" {
  type        = string
  description = "Username for ssh and such"
  default     = "che"
}

variable "tpz_username" {
  type        = string
  description = "Username for The Project Zone (to turn in assignment)"
  default     = "colavarr@andrew.cmu.edu"
}

variable "tpz_password" {
  type        = string
  description = "Password for The Project Zone (to turn in assignment)"
  default     = "2gtozqZwQPuwvOo3Gmdonl"
}

terraform {
  backend "remote" {
    organization = "cloud-computing"
    workspaces {
      name = "project-0_gcp"
    }
  }
}

provider "google" {
  credentials = file("compute-admin-credentials.json")
  project     = "indigo-pod-303903"
  region      = "us-east1"
  zone        = "us-east1-c" # Zone needs to be specified here or otherwise
}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
  name         = "test-vm"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      size  = "10"          # This is the default
      type  = "pd-standard" # This is the default
      image = "ubuntu-os-cloud/ubuntu-2004-focal-v20210129"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Include this section to give the VM an external ip address
    }
  }
  # This is "networking tags". One way the firwall works is by apply rules to
  # tagged instances. One rule for instance is to allow ingress on port 80 from
  # anywhere to any server with the 'http-server' tag
  tags = ["http-server", "https-server"]

  metadata = {
    ssh-keys  = "che:${file(var.pub_key)}"
    user-data = templatefile(
      "user-data.yaml", {
        username     = var.username
        tpz_username = var.tpz_username
        tpz_password = var.tpz_password
      }
    )
  }

  labels = {
    "project" = var.project_num
  }

  connection {
    type        = "ssh"
    user        = var.username
    private_key = file(var.priv_key)
    host        = self.network_interface.0.access_config.0.nat_ip
  }

  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
      "TPZ_USERNAME=${var.tpz_username} TPZ_PASSWORD=${var.tpz_password} bash -c 'echo \"I AGREE\" | /home/${var.username}/submitter ${self.network_interface.0.access_config.0.nat_ip}' || true",
    ]
  }
}

output "ip" {
  value = google_compute_instance.default.network_interface.0.access_config.0.nat_ip
}

output "domain_name" {
  value = google_compute_instance.default.network_interface.0.access_config.0.public_ptr_domain_name
}

#output "user-data" {
#  value = google_compute_instance.default.metadata.user-data
#}

/*
data "google_compute_image" "ubuntu_20" {
  family = "ubuntu-2004-lts"
  project = "ubuntu-os-cloud"
}

output "image" {
  value = data.google_compute_image.ubuntu_20.name
}
*/
